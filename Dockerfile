FROM lachlanevenson/k8s-kubectl

RUN apk add --update ca-certificates \
 && apk add --update -t deps curl \
 && apk add git \
 && curl -s https://api.github.com/repos/kubernetes-sigs/kustomize/releases |\
        grep browser_download |\
        grep linux |\
        cut -d '"' -f 4 |\
        grep /kustomize/v |\
        sort | tail -n 1 |\
        xargs curl -sL  |\
        tar xz \
 && install -m 755 kustomize /usr/local/bin/kustomize \
 && rm -rf ./kustomize \
 && apk del --purge deps \
 && rm /var/cache/apk/*
